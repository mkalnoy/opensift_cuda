#include <opencv\cv.h>
#include <opencv\highgui.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include "imgfeatures.h"

struct cuda_launch_options {

	dim3 blocks, threads;
};

struct convolution_kernel_t {

	float * data;

	int r;

	bool device_alloc;
};

struct image_wrapper_t {

	float * data;

	int w, h;

};

struct gss_t {

	IplImage *** gss_pyr, *** dog_pyr;
};

struct octave_t {

	int idx;

	int w, h;

	IplImage ** gss_oct, ** dog_oct;
};

struct workspace_t {

	bool initialised;

	/* Sift properties */

	IplImage * image;

	int octaves, scales;

	double sigma;

	double curv_thr, contr_thr;

	int descr_width, descr_hist_bins;

	int double_size;

	int features_count;

	feature * features;

	/* Gpu access */

	int device;

	int current;

	int buffer_w, buffer_h;

	image_wrapper_t base, fst_buff, scnd_buff, d_buff;

	cudaStream_t main;

};

convolution_kernel_t * get_kernel(double sigma, bool deviceAlloc = true);

extern image_wrapper_t * downsample(image_wrapper_t * src);

extern image_wrapper_t * upsample(image_wrapper_t * src);

extern void downsample_async(image_wrapper_t * src, image_wrapper_t * dst, workspace_t * workspace);

extern void upsample_async(image_wrapper_t * src, image_wrapper_t * dst, workspace_t * workspace);

extern gss_t build_cuda_gss(IplImage * base, double sigma, int octaves, int scales);

extern void init_device(double sigma, int scale_range, int device_id);

extern void init_workspace(workspace_t * workspace);

extern void sync_workspace(workspace_t * workspace);

extern void reset_workspace(workspace_t * workspace);

extern void deallocate_workspace_buffers(workspace_t * workspace);

extern void destroy_workspace(workspace_t * workspace);

extern octave_t * next_octave(workspace_t * workspace);

extern void destroy_octave(octave_t * octave, workspace_t * workspace);

extern void add_features(workspace_t * workspace, CvSeq * seq);

extern int get_cuda_device_count();