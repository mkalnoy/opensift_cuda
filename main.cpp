/*
  This program detects image features using SIFT keypoints. For more info,
  refer to:
  
  Lowe, D. Distinctive image features from scale-invariant keypoints.
  International Journal of Computer Vision, 60, 2 (2004), pp.91--110.
  
  Copyright (C) 2006-2012  Rob Hess <rob@iqengines.com>

  Note: The SIFT algorithm is patented in the United States and cannot be
  used in commercial products without a license from the University of
  British Columbia.  For more information, refer to the file LICENSE.ubc
  that accompanied this distribution.

  Version: 1.1.2-20100521
*/

#include "sift.h"
#include "imgfeatures.h"
#include "utils.h"

#include <opencv\cv.h>
#include <opencv\highgui.h>

#include <vector>

#include "cudawrapper.h"

#include <Windows.h>

//#include <getopt.h>

/******************************** Globals ************************************/

char* pname;
char* img_file_name;
char* out_file_name = NULL;
char* out_img_name = NULL;
int intvls = SIFT_INTVLS;
double sigma = SIFT_SIGMA;
double contr_thr = SIFT_CONTR_THR;
int curv_thr = SIFT_CURV_THR;
int img_dbl = SIFT_IMG_DBL;
int descr_width = SIFT_DESCR_WIDTH;
int descr_hist_bins = SIFT_DESCR_HIST_BINS;
int display = 1;


/********************************** Main *************************************/

int main( int argc, char** argv ) {

	if(argc != 3) {
		
		std::cout << "Invalid arguments." << std::endl;
		std::cout << "Usage : " << std::endl;
		std::cout << "[1] Input  lists. One image path a row." << std::endl;
		std::cout << "[2] Output lists. One image path a row." << std::endl;

		return 1;
	}

	std::vector<string> paths, out_paths;

	if(argc == 3) {

		paths = parse_file(argv[1]);

		//for(int i = 0; i < paths.size(); i ++) std::cout << paths.at(i) << std::endl;

		out_paths = parse_file(argv[2]);

		//for(int i = 0; i < out_paths.size(); i ++) std::cout << out_paths.at(i) << std::endl;		
	}

	LARGE_INTEGER start, end, frequency;

	QueryPerformanceCounter(&start);
	QueryPerformanceFrequency(&frequency);

	_sift_multi_features(paths, out_paths, intvls, sigma, contr_thr, curv_thr,
		      img_dbl, descr_width, descr_hist_bins );

	QueryPerformanceCounter(&end);

	double time = (double)((double)(end.QuadPart - start.QuadPart) / (double)frequency.QuadPart);

	std::cout << "Elapsed : " << time << std::endl << std::endl;

	//return 0;

	QueryPerformanceCounter(&start);
	QueryPerformanceFrequency(&frequency);

//#pragma omp parallel for

	for(int i = 0; i < paths.size(); i ++) {

		IplImage* img;

		struct feature * features;

		int n = 0;

		//fprintf( stderr, "Finding SIFT features...\n" );
		
		img = cvLoadImage( paths[i].c_str(), 1 );

		if( ! img ) fatal_error( "unable to load image from %s", img_file_name );

		n = _sift_features( img, &features, intvls, sigma, contr_thr, curv_thr,
					img_dbl, descr_width, descr_hist_bins );

		fprintf( stderr, "Found %d features.\n", n );

		export_features( (char *)out_paths[i].c_str(), features, n );

		free(features);
	}

	QueryPerformanceCounter(&end);

	time = (double)((double)(end.QuadPart - start.QuadPart) / (double)frequency.QuadPart);

	std::cout << "Elapsed : " << time << std::endl << std::endl;

	return 0;
}
