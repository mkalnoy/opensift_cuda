#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cudawrapper.h"
#include "sift.h"

#include <cmath>

#include <stdio.h>

#include <iostream>

image_wrapper_t * wrap_iplimage(IplImage * image) {

	if(image->nChannels != 1) return 0;

	image_wrapper_t * wrap = (image_wrapper_t *)malloc(sizeof(image_wrapper_t));

	wrap->w = image->width;
	wrap->h = image->height;

	float * data;

	cudaMalloc((void **)&data, sizeof(float) * wrap->w * wrap->h);

	cudaMemcpy(data, image->imageData, sizeof(float) * wrap->w * wrap->h, cudaMemcpyHostToDevice);

	wrap->data = data;

	return wrap;
}

IplImage * unwrap_iplimage(image_wrapper_t * wrap) {

	float * data;

	cudaHostAlloc((void **)&data, sizeof(float) * wrap->w * wrap->h, cudaHostAllocDefault);

	IplImage * image = cvCreateImageHeader(cvSize(wrap->w, wrap->h), IPL_DEPTH_32F, 1);

	image->imageData = (char *)data;

	cudaMemcpy(data, wrap->data, sizeof(float) * wrap->w * wrap->h, cudaMemcpyDeviceToHost);

	return image;
}

void init_wrapper(image_wrapper_t * wrap, int w, int h) {

	wrap->w = w;
	wrap->h = h;

	cudaMalloc((void **) & wrap->data, sizeof(float) * w * h);
}

void delete_wrapper(image_wrapper_t * target) {

	cudaFree(target->data);
}

void delete_convolution_kernel(convolution_kernel_t * target) {

	if(target->device_alloc) cudaFree(target->data);

	else cudaFreeHost(target->data);
}

void save_iplimage(char * path, IplImage * image) {

	IplImage * i = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);
	
	cvConvertScale(image, i, 255.0, 0);

	std::cerr << "Save " << path << std::endl;

	cvSaveImage(path, i);
}

void save_wrapper(char * path, image_wrapper_t * wrap) {

	IplImage * image = unwrap_iplimage(wrap); 

	save_iplimage(path, image);
}

float * get_cv_kernel(int r, double sigma) {

	 int w = 2 * r + 1;    
	 
	 float* kernel = (float*)malloc(sizeof(float) * w);
	
	 double scale2X = -0.5 / (sigma * sigma);

	 int i;

	 for( i = 0; i < w; i++ )
     {
        double x = i - (w - 1) * 0.5;
		double t = std::exp(scale2X * x * x);        

	    kernel[i] = (float)t;
	 }

	 //print_kernel(kernel, w);
	
    return kernel;
}

convolution_kernel_t * get_kernel(double sigma, bool deviceAlloc) {

	float value, k_sum = 0;

	int r = floor(3.0 * sigma + 0.5);

	//std::cout << "Sigma = " << sigma << " R = " << r << std::endl;

	int w = r * 2 + 1;

	float * k_1d = get_cv_kernel(r, sigma);

	//float * k_2d = (float *)malloc(sizeof(float) * w * w);

	float * k_2d;

	cudaHostAlloc((void **) & k_2d, sizeof(float) * w * w, cudaHostAllocDefault);

	//std::cout << "Allocating kernel via cudaHostAlloc : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	for(int i = 0; i < w; i ++)

		for(int j = 0; j < w; j ++) {

			value = k_1d[i] * k_1d[j];

			k_2d[i * w + j] = value;

			k_sum += value;
		}

	k_sum = 1.f / k_sum;

	for(int i = 0; i < w * w; i ++) k_2d[i] = k_2d[i] * k_sum;

	//print_kernel(k_values, w);

	float * data;

	if(deviceAlloc) {

		cudaMalloc((void **)&data, sizeof(float) * w * w);

		cudaMemcpy(data, k_2d, sizeof(float) * w * w, cudaMemcpyHostToDevice);
	}

	else {

		data = k_2d;
	}

	convolution_kernel_t * kernel = (convolution_kernel_t *)malloc(sizeof(convolution_kernel_t));

	kernel->r    = r;
	kernel->data = data;
	kernel->device_alloc = deviceAlloc;

	return kernel;	
}

int get_cuda_device_count() {

	int devices;

	cudaGetDeviceCount(&devices);

	return devices;
}

#define MAX_LENGTH 2000
#define MAX_SCALES 8

__device__ __constant__ float kernel[MAX_LENGTH];
__device__ __constant__ int	  offset[MAX_SCALES];
__device__ __constant__ int		   r[MAX_SCALES];

__global__ void gaussian_blur_cuda_kernel_async(float * in, float * out, int w, int h, int kernel_idx) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;
	
	int t_idx = t_x + t_y * w;

	int x = t_x, y = t_y;

	int idx;

	int radius =      r[kernel_idx];
	int shift  = offset[kernel_idx];

	float value = 0.f;

	for(int i = 0; i < radius * 2 + 1; i ++) {
		for(int j = 0; j < radius * 2 + 1; j ++) {

			x = t_x + j - radius;
			y = t_y + i - radius;

			if(x < 0) x = 0;//x * (-1);
			if(y < 0) y = 0;//y * (-1);

			if(x >= w - 1) x = w - 1;// - (x - w + 1);
			if(y >= h - 1) y = h - 1;// - (y - h + 1);

			idx = x + y * w;

			value += in[idx] * kernel[j + i * (radius * 2 + 1) + shift];
		}
	}

	out[t_idx] = value;
}

__global__ void gaussian_blur_cuda_kernel(float * in, float * out, float * bkernel, int br, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;
	
	int t_idx = t_x + t_y * w;

	int x = t_x, y = t_y;

	int idx;

	float value = 0.f;

	for(int i = 0; i < br * 2 + 1; i ++) {
		for(int j = 0; j < br * 2 + 1; j ++) {

			x = t_x + j - br;
			y = t_y + i - br;

			if(x < 0) x = 0;//x * (-1);
			if(y < 0) y = 0;//y * (-1);

			if(x >= w - 1) x = w - 1;// - (x - w + 1);
			if(y >= h - 1) y = h - 1;// - (y - h + 1);

			idx = x + y * w;

			value += in[idx] * bkernel[j + i * (br * 2 + 1)];
		}
	}

	out[t_idx] = value;
}

__global__ void difference_of_gaussian_cuda_kernel(float * minuend, float * subtr, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int idx = t_x + t_y * w;

	out[idx] = minuend[idx] - subtr[idx];
}

cuda_launch_options get_gaussian_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	int max_ratio = 32;

	int w_ratio = 2, h_ratio = 2;

	for(w_ratio = 1; ; w_ratio *= 2) {

		if(w % w_ratio) {

			w_ratio /= 2;

			break;
		}
	}

	for(h_ratio = 1; ; h_ratio *= 2) {

		if(h % h_ratio) {

			h_ratio /= 2;

			break;
		}
	}

	int ratio = std::min(std::min(w_ratio, h_ratio), max_ratio);

	option.blocks  = dim3(w / ratio, h / ratio);
	option.threads = dim3(ratio, ratio);

	return option;
}

image_wrapper_t * gaussian_blur(image_wrapper_t * src, double sigma) {

	/* Allocating appropriate convolution kernel*/

	convolution_kernel_t * kernel = get_kernel(sigma);

	//std::cerr << "Kernel sigma = " << sigma << " r = " << kernel->r << std::endl;

	/* Allocating host image wrapper*/

	image_wrapper_t * blurred = (image_wrapper_t *)malloc(sizeof(image_wrapper_t));

	blurred->w = src->w;
	blurred->h = src->h;

	//std::cerr << "Allocated " << src->w << "x" << src->h << std::endl;

	float * data;

	cudaMalloc((void **)&data, sizeof(float) * src->w * src->h);

	//std::cout << "cudaMalloc : " << cudaGetErrorString(cudaGetLastError()) << std::endl;	

	/* Getting cuda kernel params*/

	cuda_launch_options op = get_gaussian_kernel_configuration(src->w, src->h);

	//std::cout << "Launch : blur | " << "Blocks : " << op.blocks.x << "x" << op.blocks.y 
		
		//<< " | Threads : " << op.threads.x << "x" << op.threads.y << std::endl;

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	//std::cout << "Before exec : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	gaussian_blur_cuda_kernel<<<op.blocks, op.threads>>>(src->data, data, kernel->data, kernel->r, src->w, src->h);

	std::cerr << "Gaussian kernel : " << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	//std::cout << "After exec : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	blurred->data = data;

	return blurred;

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;
}

void gaussian_blur_async(image_wrapper_t * src, image_wrapper_t * dst, workspace_t * workspace, int idx) {

	cuda_launch_options op = get_gaussian_kernel_configuration(src->w, src->h);

	dst->w = src->w;
	dst->h = src->h;

	//std::cout << "Src w = " << src->w << " Src h = " << src->h << std::endl;

	//std::cout << "Dst w = " << dst->w << " Dst h = " << dst->h << std::endl;

	//std::cout << "Launch : blur | " << "Blocks : " << op.blocks.x << "x" << op.blocks.y 
		
		//<< " | Threads : " << op.threads.x << "x" << op.threads.y << std::endl;

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	//std::cout << "Before exec : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	gaussian_blur_cuda_kernel_async<<<op.blocks, op.threads, 0, workspace->main>>>(
		
		src->data, dst->data, src->w, src->h, idx);

	//std::cerr << "Gaussian kernel : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	//std::cout << "After exec : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;
}

cuda_launch_options get_dog_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	cudaDeviceProp device_property;

	cudaGetDeviceProperties(&device_property, 0);

	option.threads = dim3(8, 8);

	option.blocks = dim3(w / option.threads.x, h / option.threads.y);

	return option;

}

image_wrapper_t * difference_of_gaussian(image_wrapper_t * minuend, image_wrapper_t * subtr) {

	int w = minuend->w;
	int h = minuend->h;
	
	float * difference;

	cudaMalloc((void **)&difference, sizeof(float) * w * h);

	cuda_launch_options op = get_dog_kernel_configuration(w, h);

	//std::cout << "Launch : differ | " << "Blocks : " << op.blocks.x << "x" << op.blocks.y 
		
		//<< " | Threads : " << op.threads.x << "x" << op.threads.y << std::endl;

	difference_of_gaussian_cuda_kernel<<<op.blocks, op.threads>>>(minuend->data, subtr->data, difference, w, h);

	//std::cout << cudaGetErrorString(cudaGetLastError()) << std::endl;

	//std::cout << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	image_wrapper_t * image = (image_wrapper_t *)malloc(sizeof(image_wrapper_t));

	image->w = w;
	image->h = h;

	image->data = difference;

	return image;

}

void difference_of_gaussian_async(image_wrapper_t * minuend, image_wrapper_t * subtr, image_wrapper_t * dst, workspace_t * workspace) {

	cuda_launch_options op = get_gaussian_kernel_configuration(minuend->w, minuend->h);

	//std::cout << "Launch : differ | " << "Blocks : " << op.blocks.x << "x" << op.blocks.y 
		
		//<< " | Threads : " << op.threads.x << "x" << op.threads.y << std::endl;

	difference_of_gaussian_cuda_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(
		
		minuend->data, subtr->data, dst->data, minuend->w, minuend->h);

}

gss_t build_cuda_gss(IplImage * base, double sigma, int octaves, int scales) {

	//std::cerr << "In cuda part" << std::endl;

	cudaEvent_t start, end;

	float elapsedTime;

	cudaEventCreate(&start);
	cudaEventCreate(&end);

	cudaEventRecord(start, 0);
	
	float * base_img_data;

	//std::cerr << "Octaves = " << octaves << " Scales = " << scales + 3 << std::endl;

	cudaMalloc((void **)&base_img_data, sizeof(float) * base->width * base->height);

	//std::cerr << "Copying" << std::endl;

	cudaMemcpy(base_img_data, base->imageData, sizeof(float) * base->width * base->height, cudaMemcpyHostToDevice);

	//std::cerr << "Done" <<  std::endl;

	cudaDeviceSynchronize();

	image_wrapper_t * base_img = wrap_iplimage(base);

	//std::cerr << "Base img " << base_img->w << "x" << base_img->h << std::endl;	

	//std::cerr << "Assigning sigmas" << std::endl;

	double * sig, sig_total, sig_prev, k;

	sig = (double *)malloc((scales + 3) * sizeof(double));

	sig[0] = sigma;

	k = pow(2.0, 1.0 / scales);

	for(int i = 1; i < scales + 3; i ++) {

      sig_prev = pow(k, i - 1) * sigma;

      sig_total = sig_prev * k;

      sig[i] = sqrt(sig_total * sig_total - sig_prev * sig_prev);

    }

	//std::cerr << "Building empty gss" << std::endl;

	image_wrapper_t *** gss = (image_wrapper_t ***)malloc(sizeof(image_wrapper_t **) * octaves);

	for(int o = 0; o < octaves; o ++) {

		gss[o] = (image_wrapper_t **)malloc(sizeof(image_wrapper_t *) * (scales + 3));

	}

	for(int o = 0; o < octaves; o ++) {

		//cudaDeviceSynchronize();

		for(int i = 0; i < scales + 3; i ++) {
			
			if(o == 0  &&  i == 0) {
				
				gss[o][i] = base_img;
				
				//std::cerr << "Base img" << std::endl;
			}

	// base of new octvave is halved image from end of previous octave 
			else if( i == 0 ) {

				gss[o][i] = downsample(gss[o - 1][scales]);

				//cudaDeviceSynchronize();

				//std::cerr << "Downsampling [" << o << "][" << i << "]" << std::endl;
			}
	  
	//blur the current octave's last image to create the next one 
			else {

				gss[o][i] = gaussian_blur(gss[o][i - 1], sig[i]);
				//cudaDeviceSynchronize();

				//std::cerr << "Bluring [" << o << "][" << i << "]" << std::endl;
			}
		}
	}

	/* Difference of gaussian */

	image_wrapper_t *** dog = (image_wrapper_t ***)malloc(sizeof(image_wrapper_t **) * octaves);

	for(int o = 0; o < octaves; o ++) {

		dog[o] = (image_wrapper_t **)malloc(sizeof(image_wrapper_t *) * (scales + 2));

	}

	for(int o = 0; o < octaves; o ++)
		for(int i = 0; i < scales + 2; i ++) {

			dog[o][i] = difference_of_gaussian(gss[o][i + 1], gss[o][i]);
      }

	/* Building IplImage pyr */

	//std::cerr << "Allocating base" << std::endl;

	IplImage *** g = (IplImage ***)malloc(sizeof(IplImage **) * octaves);

	//std::cerr << "Bluring [" << o << "][" << i << "]" << std::endl;

	for(int o = 0; o < octaves; o ++) g[o] = (IplImage **)malloc(sizeof(IplImage *) * (scales + 3));

	for(int o = 0; o < octaves; o ++) {

		for(int i = 0; i < scales + 3; i ++) {

			g[o][i] = unwrap_iplimage(gss[o][i]);
		}
	}

	IplImage *** d = (IplImage ***)malloc(sizeof(IplImage **) * octaves);

	//std::cerr << "Bluring [" << o << "][" << i << "]" << std::endl;

	for(int o = 0; o < octaves; o ++) d[o] = (IplImage **)malloc(sizeof(IplImage *) * (scales + 2));

	for(int o = 0; o < octaves; o ++) {

		for(int i = 0; i < scales + 2; i ++) {

			d[o][i] = unwrap_iplimage(dog[o][i]);
		}
	}

	/*for(int o = 0; o < octaves; o ++)
		for(int i = 0; i < scales + 3; i ++) {

			char * file_name = new char[30];

			sprintf(file_name, "D:/Gss/octave_%i_scale_%i.bmp", o, i);

			//save_wrapper(file_name, gss[o][i]);

			save_iplimage(file_name, g[o][i]);
		}*/

	for(int o = 0; o < octaves; o ++) {

		for(int i = 0; i < scales + 3; i ++) delete_wrapper(gss[o][i]);

		for(int i = 0; i < scales + 2; i ++) delete_wrapper(dog[o][i]);
	}


	gss_t rs;

	rs.gss_pyr = g;
	rs.dog_pyr = d;

	cudaEventRecord(end, 0);
	cudaEventSynchronize(end);

	cudaEventElapsedTime(&elapsedTime, start, end);

	std::cerr << "Cuda elapsed " << elapsedTime / (double)1000 << std::endl;

	return rs;
}

void init_workspace_buffers(workspace_t * workspace) {

	int w, h;

	if(workspace->double_size) {

		w = workspace->image->width  * 2;
		h = workspace->image->height * 2;		
	}
	
	else {

		w = workspace->image->width;
		h = workspace->image->height;
	}

	workspace->buffer_w = w;
	workspace->buffer_h = h;

	init_wrapper( & workspace->base, w, h);

	//std::cout << "Alloc base : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	init_wrapper( & workspace->fst_buff, w, h);

	//std::cout << "Alloc 1st buff : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	init_wrapper( & workspace->scnd_buff, w, h);

	//std::cout << "Alloc 2nd buff : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	init_wrapper( & workspace->d_buff, w, h);
}

void init_workspace(workspace_t * workspace) {

	if(workspace->initialised) {

		if(workspace->image->width > workspace->buffer_w || workspace->image->height > workspace->buffer_h) {

			//std::cout << "Reinit ws" << std::endl;

			deallocate_workspace_buffers(workspace);

			init_workspace_buffers(workspace);
		}
	}

	else {

		cudaSetDevice(workspace->device);

		//std::cout << "Init ws" << std::endl;

		init_workspace_buffers(workspace);

		cudaStreamCreate( &(workspace->main) );

		workspace->initialised = true;
	}

	reset_workspace(workspace);

	//Prebuild image <...>

	cudaHostRegister(
		
		workspace->image->imageData, 
		
		sizeof(float) * workspace->image->width * workspace->image->height, 
		
		cudaHostRegisterPortable);

	if(workspace->double_size) {

		//std::cout << "Size doubling" << std::endl;

		workspace->fst_buff.w = workspace->image->width;
		workspace->fst_buff.h = workspace->image->height;

		cudaMemcpyAsync(
		
			workspace->fst_buff.data, workspace->image->imageData, 
		
			sizeof(float) * workspace->image->width * workspace->image->height, 
		
			cudaMemcpyHostToDevice, workspace->main);

		upsample_async(&workspace->fst_buff, &workspace->scnd_buff, workspace);

		gaussian_blur_async(&workspace->scnd_buff, &workspace->base, workspace, 0);

		/*cudaStreamSynchronize(workspace->main);

		save_wrapper("D:/tmp.jpg", &workspace->base);*/
	}

	else {

		cudaMemcpyAsync(
		
			workspace->base.data, workspace->image->imageData, 
		
			sizeof(float) * workspace->image->width * workspace->image->height, 
		
			cudaMemcpyHostToDevice, workspace->main);
	}

	//std::cout << "Copying image : " << cudaGetErrorString(cudaGetLastError()) << std::endl;
}

void destroy_workspace(workspace_t * workspace) {

	if(!workspace->initialised) return;

	deallocate_workspace_buffers(workspace);

	//if(workspace->image) cvReleaseImage(&workspace->image);
}

void init_device(double sigma, int scale_range, int device_id) {

	cudaSetDevice(device_id);

	int scales = scale_range;

	double * sig, sig_total, sig_prev, k;

	sig = (double *)malloc((scales + 3) * sizeof(double));

	sig[0] = sigma;

	k = pow(2.0, 1.0 / scales);

	for(int i = 1; i < scales + 4; i ++) {

      sig_prev = pow(k, i - 1) * sigma;

      sig_total = sig_prev * k;

      sig[i] = sqrt(sig_total * sig_total - sig_prev * sig_prev);
    }

	int * kernel_offsets  = (int *)calloc(scales + 3, sizeof(int));
	int * kernel_radiuses = (int *)calloc(scales + 3, sizeof(int));

	int kernel_length = 0;

	for(int i = 0; i < scales + 3; i ++) {

		convolution_kernel_t * conv;

		if(!i) {

			conv = get_kernel(sqrt(sigma * sigma - SIFT_INIT_SIGMA * SIFT_INIT_SIGMA), false);
		}

		else {

			conv = get_kernel(sig[i], false);
		}

		//std::cout << "sigma = " << sig[i] << std::endl;

		//std::cout << "r = " << conv->r << std::endl;

		int size = (2 * conv->r + 1) * (2 * conv->r + 1);

		//std::cout << "size = " << size << std::endl;		

		cudaMemcpyToSymbol(kernel, conv->data, sizeof(float) * size, sizeof(float) * kernel_length, cudaMemcpyHostToDevice);

		//std::cout << "Partial copy : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

		//std::cout << "copy to -> " << kernel_length << std::endl;

		//std::cout << "Alloc [" << i << "] kernel : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

		kernel_offsets [i] = kernel_length;
		kernel_radiuses[i] = conv->r;

		kernel_length += size;
	}

	cudaMemcpyToSymbol(offset, kernel_offsets,  sizeof(int) * (scales + 3), 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(r,	   kernel_radiuses, sizeof(int) * (scales + 3), 0, cudaMemcpyHostToDevice);

	//std::cout << "Alloc offsets : " << cudaGetErrorString(cudaGetLastError()) << std::endl;
}

void init_octave(octave_t * octave, workspace_t * workspace) {

	//std::cout << "Init octave" << std::endl;

	octave->w = workspace->current ? workspace->base.w / 2 : workspace->base.w;
	octave->h = workspace->current ? workspace->base.h / 2 : workspace->base.h;

	octave->idx = workspace->current;

	octave->gss_oct = (IplImage **)calloc(workspace->scales + 3, sizeof(IplImage *));
	octave->dog_oct = (IplImage **)calloc(workspace->scales + 2, sizeof(IplImage *));

	for(int i = 0; i < workspace->scales + 3; i ++) {

		octave->gss_oct[i] = cvCreateImageHeader(cvSize(octave->w, octave->h), IPL_DEPTH_32F, 1);

		float * ptr = 0;

		cudaHostAlloc((void **)&ptr, sizeof(float) * octave->w * octave->h, cudaHostAllocDefault);

		octave->gss_oct[i]->imageData = (char *)ptr;
	}

	for(int i = 0; i < workspace->scales + 2; i ++) {

		octave->dog_oct[i] = cvCreateImageHeader(cvSize(octave->w, octave->h), IPL_DEPTH_32F, 1);

		float * ptr = 0;

		cudaHostAlloc((void **)&ptr, sizeof(float) * octave->w * octave->h, cudaHostAllocDefault);

		octave->dog_oct[i]->imageData = (char *)ptr;
	}

	//std::cout << "Init octave end : " << cudaGetErrorString(cudaGetLastError()) << std::endl;
}

 void destroy_octave(octave_t * octave, workspace_t * workspace) {

	 for(int i = 0; i < workspace->scales + 3; i ++) {

		 cudaFreeHost(octave->gss_oct[i]->imageData);

		 cvReleaseImageHeader(&octave->gss_oct[i]);
	}

	for(int i = 0; i < workspace->scales + 2; i ++) {

		 cudaFreeHost(octave->dog_oct[i]->imageData);

		 cvReleaseImageHeader(&octave->dog_oct[i]);
	}
}

octave_t * next_octave(workspace_t * workspace) {

	if(workspace->current == workspace->octaves) return 0;

	image_wrapper_t * previous, * current, * dog, * base, * tmp;

	previous = & workspace->scnd_buff;
	current	 = & workspace->fst_buff;
	dog		 = & workspace->d_buff;

	octave_t * octave = (octave_t *)malloc(sizeof(octave_t));

	init_octave(octave, workspace);

	if(workspace->current) downsample_async(& workspace->base, previous, workspace);

	else cudaMemcpyAsync(previous->data, workspace->base.data, workspace->base.w * workspace->base.h * sizeof(float), cudaMemcpyDeviceToDevice, workspace->main);	

	cudaMemcpyAsync(octave->gss_oct[0]->imageData, previous->data, previous->w * previous->h * sizeof(float), cudaMemcpyDeviceToHost, workspace->main);

	//cudaStreamSynchronize(workspace->main);

	for(int i = 1; i < workspace->scales + 3; i ++) { //Attention here		

		gaussian_blur_async(previous, current, workspace, i);

		//cudaStreamSynchronize(workspace->main);

		difference_of_gaussian_async(current, previous, dog, workspace);

		cudaMemcpyAsync(octave->gss_oct[i]->imageData, current->data, current->w * current->h * sizeof(float), cudaMemcpyDeviceToHost, workspace->main);

		if(i == workspace->scales) {
			
			cudaMemcpyAsync(workspace->base.data, current->data, current->w * current->h * sizeof(float), cudaMemcpyDeviceToDevice, workspace->main);

			workspace->base.w = current->w;
			workspace->base.h = current->h;		
		}

		cudaMemcpyAsync(octave->dog_oct[i - 1]->imageData, dog->data, current->w * current->h * sizeof(float), cudaMemcpyDeviceToHost, workspace->main);

		tmp = previous;

		previous = current;

		current = tmp;

		tmp = 0;
	}

	/*cudaError_t er = cudaStreamSynchronize(workspace->main);

	std::cout << "Exec : " << cudaGetErrorString(er) << std::endl;

	for(int i = 0; i < workspace->scales + 3; i ++) {

		char * file_name = new char[30];

		sprintf(file_name, "D:/Gss/octave_%i_scale_%i.bmp", workspace->current, i);

		save_iplimage(file_name, octave->gss_oct[i]);
	}*/

	workspace->current ++;

	return octave;
}

void sync_workspace(workspace_t * workspace) {

	cudaError_t er = cudaStreamSynchronize(workspace->main);

	//std::cout << "Exec : " << cudaGetErrorString(er) << std::endl;
}

extern void add_features(workspace_t * workspace, CvSeq * seq) {

	int n = seq->total;

	if(!workspace->features_count) {

		workspace->features = (feature *)calloc(n, sizeof(feature));

		workspace->features_count = n;

		cvCvtSeqToArray(seq, workspace->features, CV_WHOLE_SEQ);
	}
	
	else {

		//std::cout << "Reallocing" << std::endl;

		workspace->features = (feature *)realloc(workspace->features, sizeof(feature) * (workspace->features_count + n));

		cvCvtSeqToArray(seq, workspace->features + workspace->features_count, CV_WHOLE_SEQ);

		workspace->features_count += n;
	}
}

extern void reset_workspace(workspace_t * workspace) {

	workspace->current = 0;

	workspace->features_count = 0;

	if(workspace->features_count) {
		
		free(workspace->features);

		workspace->features = 0;
	}
}

extern void deallocate_workspace_buffers(workspace_t * workspace) {

	delete_wrapper(&workspace->fst_buff );
	delete_wrapper(&workspace->scnd_buff);
	delete_wrapper(&workspace->d_buff   );
	delete_wrapper(&workspace->base     );
}

