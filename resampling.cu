#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <math.h>

#include "cudawrapper.h"

__constant__ __device__ int radius = 1, width = 3;

__constant__ __device__ float kernel[3] = {0.5, 1, 0.5};

__global__ void resize_kernel(float * in, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	float value = in[t_x + t_y * w];

	t_x *= 2;
	t_y *= 2;

	out[t_x + t_y * w * 2] = value;

	out[t_x + 1 + t_y * w * 2] = out[t_x + (t_y + 1) * w * 2] = out[t_x + 1 + (t_y + 1) * w * 2] = 0.f;
}

__global__ void x_interpolation_kernel(float * data, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x, y;

	float value = 0.f;

	for(int i = 0; i < width; i ++) {

		x = t_x * 2 + i - radius + 1;
		
		y = t_y * 2;

		if(x < 0) x = 0;

		if(x >= w - 1) x = w - 1;

		value += kernel[i] * data[x + y * w];
	}

	data[t_x * 2 + 1 + (t_y * 2) * w] = value;
}

__global__ void y_interpolation_kernel(float * data, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x, y;

	float value = 0.f;

	for(int i = 0; i < width; i ++) {

		y = t_y * 2 + i - radius + 1;
		
		x = t_x;

		if(y < 0) y = 0;

		if(y >= h - 1) y = h - 1;

		value += kernel[i] * data[x + y * w];
	}

	data[t_x + (t_y * 2 + 1) * w] = value;
}

__global__ void slice_kernel(float * in, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x = t_x * 4 + 1;
	int y = t_y * 4 + 1;

	out[t_x + t_y * w / 4] = in[x + y * w];
}

__global__ void downsample_kernel(float * in, float * out, int srcW, int srcH) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	float value = 0;

	for(int x = 0; x < 2; x ++)
		for(int y = 0; y < 2; y ++) {

			value += in[t_x * 2 + x + (t_y * 2 + y) * srcW];
		}

	out[t_x + t_y * srcW / 2] = value / 4.f;

}

cuda_launch_options get_resize_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(8, 8);
	option.blocks = dim3(w / 16, h / 16);

	return option;
}

cuda_launch_options get_x_interpolation_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (2 * option.threads.x), h / (2 * option.threads.y));

	return option;
}

cuda_launch_options get_y_interpolation_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (option.threads.x), h / (2 * option.threads.y));

	return option;
}

cuda_launch_options get_slice_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (4 * option.threads.x), h / (4 * option.threads.y));

	return option;
}


void upsample_async(image_wrapper_t * src, image_wrapper_t * dst, workspace_t * workspace) {

	dst->w = src->w * 2;
	dst->h = src->h * 2;

	cuda_launch_options op = get_resize_kernel_configuration(dst->w, dst->h);

	resize_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(src->data, dst->data, src->w, src->h);

	op = get_x_interpolation_kernel_configuration(dst->w, dst->h);

	x_interpolation_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(dst->data, dst->w, dst->h);

	op = get_y_interpolation_kernel_configuration(dst->w, dst->h);

	y_interpolation_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(dst->data, dst->w, dst->h);
}

image_wrapper_t * downsample(image_wrapper_t * image) {

	float * resized_data, * data;

	cudaMalloc((void **)&data, sizeof(float) * image->w * image->h / 4);

	dim3 threads = dim3(4, 4);

	dim3 blocks  = dim3(image->w / 8, image->h / 8);

	downsample_kernel<<<blocks, threads>>>(image->data, data, image->w, image->h);

	image_wrapper_t * scaled_image = (image_wrapper_t *)malloc(sizeof(image_wrapper_t));

	scaled_image->w = image->w / 2;
	scaled_image->h = image->h / 2;

	scaled_image->data = data;

	return scaled_image;

	/*cudaMalloc((void **)&resized_data, sizeof(float) * image->w * image->h * 4);

	cudaMalloc((void **)&data, sizeof(float) * image->w * image->h / 4);

	int w = image->w * 2;
	int h = image->h * 2;

	cuda_launch_options op = get_resize_kernel_configuration(w, h);

	resize_kernel<<<op.blocks, op.threads>>>(image->data, resized_data, image->w, image->h);

	op = get_x_interpolation_kernel_configuration(w, h);

	x_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	op = get_y_interpolation_kernel_configuration(w, h);

	y_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	op = get_slice_kernel_configuration(w, h);

	slice_kernel<<<op.blocks, op.threads>>>(resized_data, data, w, h);

	std::cerr << "Slicing : " << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	image_wrapper_t * scaled_image = (image_wrapper_t *)malloc(sizeof(image_wrapper_t));

	scaled_image->w = w / 4;
	scaled_image->h = h / 4;

	std::cout << "W : " << image->w << " -> " << scaled_image->w << std::endl;
	std::cout << "H : " << image->h << " -> " << scaled_image->h << std::endl;

	scaled_image->data = data;

	return scaled_image;*/
}

void downsample_async(image_wrapper_t * src, image_wrapper_t * dst, workspace_t * workspace) {

	dim3 threads = dim3(4, 4);

	dim3 blocks  = dim3(src->w / 8, src->h / 8);

	//std::cout << "W : " << src->w << " -> " << std::endl;
	//std::cout << "H : " << src->h << " -> " <<  std::endl;

	dst->w = src->w / 2;
	dst->h = src->h / 2;

	downsample_kernel<<<blocks, threads, 0, workspace->main>>>(src->data, dst->data, src->w, src->h);

	/*int w = src->w * 2;
	int h = src->h * 2;

	cuda_launch_options op = get_resize_kernel_configuration(w, h);

	resize_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(src->data, workspace->rs_buff.data, src->w, src->h);

	op = get_x_interpolation_kernel_configuration(w, h);

	x_interpolation_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(workspace->rs_buff.data, w, h);

	op = get_y_interpolation_kernel_configuration(w, h);

	y_interpolation_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(workspace->rs_buff.data, w, h);

	op = get_slice_kernel_configuration(w, h);

	slice_kernel<<<op.blocks, op.threads, 0, workspace->main>>>(workspace->rs_buff.data, dst->data, w, h);*/

}

